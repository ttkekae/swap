#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int *ft_sort(int *arr, int n)
{
	int val,j;
	for (int i = 1; i < n; i++)
	{
		j = i - 1;
		val = arr[i];
		while (j >= 0 && val < arr[j])
		{
			arr[j + 1] = arr[j];
			j -= 1;
		}
		arr[j + 1] = val;
	}
	return arr;
}
int main()
{
	int n;
	//n = 50;
	 /*int arr[] = {	25,	17,	15,	10,	25,
		   	48,	24,	21,	37,	35,
			11,	20,	39,	19,	35,
			5,	13,	21,	40,	41,
			13,	1,	3,	40,	48,
			31,	42,	45,	35,	32,
			6,	39,	45,	43,	43,
			25,	36,	50,	3,	24,
			18,	1,	44,	25,	7,
			15,	38,	13,	2,	9 };*/
	int arr[] ={1,4, 5,8, 9, 3,2, 6, 7,10};
	n = 10;
	 ft_sort(arr,n );
	for (int i = 0; i < n; i++ )
		printf(" %i ",arr[i]);
	return 0;
}

